class Node:

    def __init__(self, leaf=True):
        self.children = {}
        self.leaf = leaf
        self.bucket = {}


class HashTree:

    def __init__(self, maxBucketSize, maxChildren):
        self.root = Node()
        self.maxBucketSize = maxBucketSize
        self.maxChildren = maxChildren

    def insert(self, node, itemset, index):
        self.index = index
        while node is not None:
            # Current node is a leaf node
            if node.leaf:
                node.bucket[itemset] = 0
                if node.bucket.__len__() == self.maxBucketSize:
                    for oitemset in node.bucket:
                        hash = self.hash(oitemset[index])
                        if hash not in node.children:
                            node.children[hash] = Node()
                        node.children[hash].bucket[oitemset] = 0
                    node.leaf = False

            # Current node is Intermediate node
            else:
                hash = self.hash(itemset[index])
                if hash not in node.children:
                    node.children[hash] = Node()
                    node.children[hash].bucket[itemset] = 0
                    break
                else:
                    node = node.children[hash]
                    self.index += 1

    def hash(self, value):
        return value % self.maxChildren


def start(candidate_itemsets, maxBucket, maxChildren):
    hashtree = HashTree(maxBucket,maxChildren)
    for itemset in candidate_itemsets:
        itemset = tuple(itemset.list)
        hashtree.insert(hashtree.root, itemset, 0)
    return hashtree




