class Rule:

    def __init__(self, lhs, rhs, supportcount, confidence):
        self.lhs = lhs
        self.rhs = rhs
        self.supportcount = supportcount
        self.confidence = confidence

    def getlhs(self):
        return self.lhs

    def getrhs(self):
        return self.rhs

    def getsupportcount(self):
        return self.supportcount

    def getconfidence(self):
        return self.confidence

    def __str__(self):
        return "Rule: " + self.lhs.__str__() + " => " + self.rhs.__str__() + " : Confidence: " + str(self.confidence)

