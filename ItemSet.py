class ItemSet:

    def __init__(self, items=None):
        if items is None:
            items = []
        self.list = items
        self.list.sort()

    def add(self, item):
        if item not in self.list:
            self.list.append(item)
            self.list.sort()

    def contains(self, items):
        for item in items:
            if item not in self.list:
                return False
        return True

    def size(self):
        return len(self.list)

    def get(self):
        return self.list

    def __str__(self):
        return "ItemSet: " + self.list.__str__()

    def __sub__(self, other):
        return self.__class__([item for item in self.list if item not in other.list])

    def __eq__(self, other):
        return self.list == other.list
