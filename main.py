from TransactionSet import TransactionSet
from HashTree import *
import itertools
from ItemSet import ItemSet
from Rule import Rule


if __name__ == "__main__":
    dataset = TransactionSet('BreadBasket_DMS.csv')
    minsupport = 30
    minconfidence = 0.5
    maxK = 5
    k = 1
    kitemsets = []
    frequentitemsets = []
    supportcounts = []

    # while [new frequent itemsets can be made]:
    kitemsets = dataset.kitemsets(k, kitemsets)
    while k <= maxK and kitemsets.__len__() > 0:
        # Calculate support of new k itemsets ---------
        support = [0] * (kitemsets.__len__())
        for i in range(0, kitemsets.__len__()):
            for key, value in dataset.transactions().items():
                if value.contains(kitemsets[i].get()):
                    support[i] += 1
        # ---------------------------------------------

        # Drop items with support too low -------------
        j = 0
        while j < kitemsets.__len__():
            if support[j] < minsupport:
                support.pop(j)
                kitemsets.pop(j)
            else:
                j += 1
        # ---------------------------------------------

        frequentitemsets += kitemsets
        supportcounts += support

        # Generate k+1-itemsets from length k frequent itemsets (with Fk-1 x Fk-1 method)
        # Prune candidate itemsets containing subsets of length k that are infrequent
        # Count the support of each itemset (with Hash Tree)
        # Eliminate candidates that are infrequent
        k += 1
        kitemsets = dataset.kitemsets(k, kitemsets)


    # Rule Generation:
    rules = []
    for i in range(frequentitemsets.__len__()):
        itemset = frequentitemsets[i]

        lrhs = 1
        while lrhs < itemset.size() and itemset.size() > 1:
            consequents = [ItemSet(list(items)) for items in itertools.combinations(itemset.get(), lrhs)]

            for consequentset in consequents:
                confidence = supportcounts[i] / supportcounts[frequentitemsets.index(consequentset)]
                if confidence >= minconfidence:
                    rules.append(Rule(itemset - consequentset, consequentset, supportcounts[i], confidence))
            lrhs += 1


    for rule in rules:
        str = "Rule: ("
        for item in rule.lhs.get():
            str += dataset.getword(item) + ","
        str += ") => ("
        for item in rule.rhs.get():
            str += dataset.getword(item) + ","
        str += ") : Confidence: " + rule.getconfidence().__str__() + " & Supportcount: " + rule.getsupportcount().__str__()
        print(str)

    """
    tree = start(dataset.kitemsets(2, dataset.oneitemsets()), 4, 3)
    print(tree)
    """

