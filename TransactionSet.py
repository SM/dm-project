import pandas as pd
import numpy as np
import itertools

from ItemSet import ItemSet


class TransactionSet:

    def __init__(self, csv_file):
        self.dict = {}
        self.items = None
        self.itemsIndexToWords = {}
        self.itemsWordsToIndex = {}
        self.loadcsv(csv_file)

    # loadcsv: load data from a csv file
    def loadcsv(self, csv_file):
        data = pd.read_csv(csv_file)
        for index, series in data.iterrows():
            tid = series["Transaction"]
            if series["Item"] != "NONE":
                if tid in self.dict and isinstance(self.dict[tid], ItemSet):
                    self.dict[tid].add(series["Item"])
                else:
                    self.dict[tid] = ItemSet([series["Item"]])
        distinctitems = self.distinctitems()
        i = 1
        for item in distinctitems:
            self.itemsIndexToWords[i] = item
            self.itemsWordsToIndex[item] = i
            i += 1

        transactions = self.dict
        self.dict = {}
        for key, value in transactions.items():
            self.dict[key] = ItemSet([])
            for transactionItem in value.get():
                self.dict[key].add(self.itemsWordsToIndex[transactionItem])
        self.calculatedistinctitems()

    # transactions: return the dict with transactions
    def transactions(self):
        return self.dict

    # size: return the number of transactions
    def size(self):
        return self.dict.__len__()

    def getword(self, index):
        return self.itemsIndexToWords[index]

    # calculatedistinctitems: calculate a list of all distinct items sold in all transactions
    def calculatedistinctitems(self):
        if self.dict is None:
            raise ValueError()
        else:
            items = {}
            for transaction in self.dict:
                for item in self.dict[transaction].get():
                    items[item] = 1

            self.items = list(items.keys())
            self.items.sort()

    def distinctitems(self):
        if self.items is None:
            self.calculatedistinctitems()
        return self.items

    def oneitemsets(self):
        return [ItemSet([item]) for item in self.distinctitems()]

    def kitemsets(self, k, itemsets):
        if k == 1:
            return self.oneitemsets()
        elif k == 2:
            distinctitems = {}
            for itemset in itemsets:
                if itemset.size() != 1:
                    raise ValueError()
                distinctitems[itemset.get()[0]] = 1
            distinctitems = list(distinctitems.keys())
            return [ItemSet(list(items)) for items in itertools.combinations(distinctitems, 2)]
        elif k >= 3:
            newitemsets = []
            # Using Fk-1 x Fk-1 method:
            for i in range(0, itemsets.__len__()):
                for j in range(i, itemsets.__len__()):
                    if itemsets[i] != itemsets[j]:
                        # Merge if their first k-2 items are identical
                        match = True
                        for l in range(0, k-2):
                            if itemsets[i].get()[l] != itemsets[j].get()[l]:
                                match = False
                                break
                        if match:
                            # Merge
                            newitemsets.append(ItemSet(itemsets[i].get()[:k-2+1] + [itemsets[j].get()[k-2]]))
            return newitemsets



